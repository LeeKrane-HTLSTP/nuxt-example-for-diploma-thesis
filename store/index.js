export const state = () => ({
  counter: 0
})

export const mutations = {
  incrementCounter: (state) => state.counter += 1
}
